﻿"use strict";
angular
    .module('userslistApp', ['ngRoute'])
    .config(config);

function config($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/users-list.html',
            controller: 'UsersListController'
        })
        .otherwise({
            redirectTo: '/'
        });
}
