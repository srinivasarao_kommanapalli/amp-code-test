﻿'use strict';
describe("Unit: Testing Facotries", function () {

    beforeEach(module('userslistApp'));
    var userslistdataservice, userlist;
    var expect = chai.expect;

    beforeEach(inject(function(_userslistdataservice_) {
        userslistdataservice = _userslistdataservice_;
    }));

    describe('userslistdataservice Test', function () {
        it('The list should have 6 users', function () {
            userslistdataservice.list(function (users) {
                userlist = JSON.parse(users);
            });
            expect(userlist.length).to.equal(6);
        });
    });
    
});