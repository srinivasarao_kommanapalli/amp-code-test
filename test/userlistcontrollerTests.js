﻿'use strict';
describe("Unit: Testing Controllers", function () {

	beforeEach(module('userslistApp'));
	var scope, controller;
	var expect = chai.expect;

	beforeEach(inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		controller = $controller('UsersListController', {
			$scope: scope
		});
	}));

	describe('UsersListController Test', function () {
		it('The scope shold have 6 users', function () {
			expect(scope.datausers.length).to.equal(6);
		});
	});

});