﻿'use strict';
describe("AMP Code Unit: Testing Modules", function () {
    beforeEach(module('userslistApp'));
    var scope, userslistdataservice, UsersListController;

    beforeEach(inject(function (_userslistdataservice_, $rootScope, $controller) {
        userslistdataservice = _userslistdataservice_;
        scope = $rootScope.$new();
        UsersListController = $controller("UsersListController", { $scope: scope });        
    }));
    
    describe('userslistApp Test', function () {
        it('should have a userslistdataservice service', function () {
            expect(userslistdataservice).to.not.equal(null);
        });

        it('should have a UsersListController controller', function () {
            expect(UsersListController).to.not.equal(null);
        });
    });
   
    
});