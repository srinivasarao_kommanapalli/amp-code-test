module.exports = function (grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        sassFiles: 'app/**/*.scss',       

        sass: {
            dist: {
                files: {
                    'build/site.css': 'app/site.scss'
                }
            }
        },
        serve: {
            path: '/build',
            options: {
                port: 7654
            },
            tasks: ['default']
        },
        watch: {
            sass: {
                tasks: ['sass'],
                files: 'app/**/*.scss'
            }

        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-serve');
    grunt.registerTask('server', ['default', 'serve']);
    grunt.registerTask('dev', ['default', 'watch']);
    grunt.registerTask('default', ['sass']);


};