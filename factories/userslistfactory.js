﻿"use strict";
angular
    .module('userslistApp')
    .factory('userslistdataservice', userslistdataservice);

userslistdataservice.$inject = ['$http'];

function userslistdataservice($http) {
    var cachedData;
    function getData(callback) {
        if (cachedData) {
            callback(cachedData);
        }
        else {
            cachedData = JSON.stringify(data);
            callback(cachedData);
        }
    }
    return {
        list: getData
    };

}