﻿"use strict";
angular
    .module('userslistApp')
    .controller('UsersListController', UsersListController);

UsersListController.$inject = ['$scope', 'userslistdataservice'];

function UsersListController($scope, userslistdataservice) {
    var vm = this;
    userslistdataservice.list(function (users) {
        $scope.datausers = JSON.parse(users);
    });
    
    $scope.sortField = 'firstName';
    $scope.reverse = true;
    return vm;
}