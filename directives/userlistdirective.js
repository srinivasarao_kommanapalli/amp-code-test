﻿"use strict";
angular
    .module('userslistApp')
    .directive('userList', userList);

function userList() {
    return {
        restrict: 'E',
        templateUrl: 'templates/userslist-Template.html'
    };
}